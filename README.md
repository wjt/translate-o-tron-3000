translate-o-tron 3000
=====================

![Robot icon for translate-o-tron 3000](https://robohash.org/translate-o-tron%203000?set=set3)

`t3k` is an **abandoned** set of tools to help maintain, review and reduce downstream translation changes in Endless OS. See [this blog post](https://blogs.gnome.org/wjjt/2019/06/21/rebasing-downstream-translations/) for some context and discussion. Since that post was published, we chose to always prefer upstream translations where present. As a result, this tool is no longer needed.

`t3k rebase`
------------

Suppose that `eos3.5` is the old downstream branch; `3.26.1` is the tag for the last upstream commit on that branch; and `eos3.6` (checked out) is the new downstream branch, based on upstream `3.32.1` (say). Then you can rebase downstream translation changes with:

```
$ cd ~/src/endlessm/gnome-control-center

# The eos3.6 branch is based on the upstream 3.32.1 tag
$ git checkout eos3.6

# Update the .pot file
$ ninja -C build meson-gnome-control-center-2.0-pot

# Update source strings in .po files
$ ninja -C build meson-gnome-control-center-2.0-update-po

# The eos3.6 branch is based on the upstream 3.26.1 tag;
# merge downstream changes between those two into the working copy
$ t3k rebase 3.26.1 eos3.5

# Optional: Python's polib formats .po files slightly differently to gettext;
# reformat them back. This has no semantic effect.
$ ninja -C build meson-gnome-control-center-2.0-update-po

$ git commit -am 'Rebase downstream translations'
```

`t3k copy`
----------

Suppose you have purchased Arabic translations for gnome-software, including many strings which are untranslated upstream. Fill these in with:

```
$ t3k copy \
    ~/src/endlessm/gnome-software/po/ar.po \
    ~/src/gnome/gnome-software/po/ar.po
```
