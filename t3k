#!/usr/bin/env python3
# vim: tw=88
# Copyright © 2019 Endless Mobile, Inc.
#
# Tools to propagate translations between 2 or more .po files.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
import argparse
import glob
import logging
import os
import polib
import re
import subprocess

log = logging.getLogger(os.path.basename(__file__))


def interesting_flags(entry):
    # Transifex never serves the 'fuzzy' flag, and sometimes apparently loses the
    # 'c-format' one.
    return set(entry.flags) - {"fuzzy", "c-format"}


def translations_equal(a, b):
    return (
        a.msgstr == b.msgstr
        and a.msgstr_plural == b.msgstr_plural
        and interesting_flags(a) == interesting_flags(b)
    )


def update_msgstrs(target, source):
    """Propagates singular and plural translations from source to target, removing the
    fuzzy flag if present on target but not on source.

    Note that target.merge(source) sounds like it might be what one wants, but actually
    POEntry.merge() updates everything *but* msgstr and msgstr_plural: the opposite of
    what we want.
    """
    if "fuzzy" in target.flags and "fuzzy" not in source.flags:
        target.flags.remove("fuzzy")

    target.msgstr = source.msgstr
    target.msgstr_plural = source.msgstr_plural


def copy(source, dest, replace_existing):
    found = 0
    entries = dest if replace_existing else dest.untranslated_entries()

    for dest_entry in entries:
        source_entry = source.find(
            dest_entry.msgid, by="msgid", msgctxt=dest_entry.msgctxt
        )
        if (
            source_entry is not None
            and source_entry.translated()
            and not translations_equal(source_entry, dest_entry)
        ):
            update_msgstrs(dest_entry, source_entry)
            found += 1

    if found:
        dest.save()
        log.info(
            "Copied %d translation(s) from %s to %s", found, source.fpath, dest.fpath
        )


def main_copy(args):
    """
    Propagates translations between .po files. Each untranslated string in 'dest'
    is looked up in 'source'; if a matching string is found, and it is translated,
    the translation is copied into 'dest'.

    This may be useful to submit downstream translations back upstream.
    """
    copy(args.source, args.dest, args.replace_existing)


def merge(base, old_downstream, target):
    """
    base: last upstream ancestor of old_downstream
    old_downstream: old downstream translation file
    target: new upstream translation, with no downstream changes

    Assumes that any downstream changes to the translation were intentional, and
    reapplies them.

    Requires that target has been regenerated to have placeholders for new strings.
    """
    found = 0

    for entry in target:
        if base is not None:
            base_entry = base.find(entry.msgid, by="msgid", msgctxt=entry.msgctxt)
        else:
            base_entry = None

        old_entry = old_downstream.find(entry.msgid, by="msgid", msgctxt=entry.msgctxt)

        # NB: .translated() is False for fuzzy translations; Transifex doesn't serve
        # fuzzy translations so this should never be an issue for old_entry.
        if old_entry is None or not old_entry.translated():
            continue
        assert "fuzzy" not in old_entry.flags

        # However, GNOME projects often do have fuzzy translations. The logic here will
        # ignore our old downstream translation when the old base translation was fuzzy;
        # probably acceptable.
        if base_entry is None or (  # this is a downstream string
            base_entry.translated()  # there was an old downstream translation
            and not translations_equal(base_entry, old_entry)  # and we changed it
        ):
            # Prefer our old downstream translation
            update_msgstrs(entry, old_entry)
            found += 1

    if found:
        target.save()
        log.info("Merged %d translations into %s", found, target.fpath)


def main_merge(args):
    """
    Looks up each source string from 'dest' in both 'base' and 'source'; when both have
    a translation, but they differ, reapplies the translation from 'source' to 'dest'.
    """
    merge(args.base, args.source, args.dest)


def git_show(git_dir, arg):
    args = ("git", "-C", git_dir, "show", arg)
    log.debug("$ %s", " ".join(args))
    return subprocess.run(
        args, capture_output=True, encoding="utf-8", check=True
    ).stdout


class PoParseError(Exception):
    pass


FIXUPS = [
    # Seems common in icon name translations
    (r'^msgstr"', 'msgstr "'),
    # Seen once in gnome-software's ca.po
    (r"^#,c-format", "#, c-format"),
]


def _parse_pofile(path_or_contents, description):
    try:
        return polib.pofile(path_or_contents)
    except IOError as exc:
        msg = f"Failed to parse {description}: {exc}"
        if path_or_contents == description:
            raise PoParseError(msg)

        log.warning(msg)
        log.warning("Trying to fix it up…")

        fixed_contents = path_or_contents
        for pattern, repl in FIXUPS:
            fixed_contents = re.sub(pattern, repl, fixed_contents, flags=re.MULTILINE)

        try:
            return polib.pofile(fixed_contents)
        except IOError as exc:
            raise PoParseError(
                f"Failed to parse {description}, even after applying fixups: {exc}"
            )


def _parse_pofile_at_rev(git_dir, rev, filename):
    rev_filename = f"{rev}:{filename}"
    try:
        contents = git_show(git_dir, rev_filename)
    except subprocess.CalledProcessError:
        return None

    return _parse_pofile(contents, rev_filename)


def rebase(args):
    """
    Compares each translated string for 'lang' at the current working copy in GIT_DIR
    between 'base_rev' and 'old_rev'; when they differ, re-applies the downstream change
    from 'old_rev' to the file in the working copy.
    """

    if not args.langs:
        langs = sorted(
            {
                os.path.splitext(os.path.basename(p))[0]
                for p in glob.glob(os.path.join(args.git_dir, "po/*.po"))
            }.difference(set(args.skip or []))
        )
    else:
        langs = args.langs

    exceptions = {}

    for lang in langs:
        log.debug("Merging %s", lang)
        try:
            rel_path = f"po/{lang}.po"
            base = _parse_pofile_at_rev(args.git_dir, args.base_rev, rel_path)
            if base is None:
                log.info(
                    "Couldn't get old upstream translation for %s; "
                    "assuming there was no upstream translation",
                    lang,
                )

            old_downstream = _parse_pofile_at_rev(args.git_dir, args.old_rev, rel_path)
            if old_downstream is None:
                log.warning(
                    "Couldn't get old downstream translation for %s; skipping", lang
                )
                continue

            target = polib.pofile(os.path.join(args.git_dir, rel_path))
            merge(base, old_downstream, target)
        except PoParseError as exc:
            log.error("While merging %s: %s", lang, exc)
            exceptions[lang] = exc

    if exceptions:
        log.error("Failed to merge %s", ", ".join(exceptions))
        exit(1)


def bad_romance(args):
    """GNOME considers French, Italian, Spanish and Portuguese to have 2 plural forms;
    Unicode's CLDR believes them to have a third, for multiples of 1,000,000.
    See <https://discourse.gnome.org/t//12867> for more discussion. This command
    finds empty plural translations in each language and fills them in from the
    version at 'upstream_rev', assuming the 'millions' form is the same as the 'other'
    form.
    """
    # https://www.youtube.com/watch?v=qrO4YZeyl0I
    for lang in args.langs:
        log.debug("Propagating %s", lang)
        found = 0

        rel_path = f"po/{lang}.po"
        upstream = _parse_pofile_at_rev(args.git_dir, args.upstream_rev, rel_path)
        if upstream is None:
            log.error(
                "Couldn't get old upstream translation for %s",
                lang,
            )

        target = polib.pofile(os.path.join(args.git_dir, rel_path))
        entries = target.untranslated_entries()

        for target_entry in target.untranslated_entries():
            if not target_entry.msgid_plural:
                continue

            upstream_entry = upstream.find(
                target_entry.msgid, by="msgid", msgctxt=target_entry.msgctxt
            )
            if upstream_entry is None:
                log.debug("No upstream match for %s", target_entry.msgid)
                continue

            log.debug("Filling in %s", target_entry.msgid)
            target_entry.msgstr_plural = {
                2: upstream_entry.msgstr_plural[1],
                **upstream_entry.msgstr_plural,
            }
            found += 1

        if found:
            target.save()
            log.info("Merged %d plural translations into %s", found, target.fpath)


def git_dir_path(string):
    """Validates that a parameter is the path to a Git checkout."""
    try:
        subprocess.run(
            ("git", "-C", string, "status"), stdout=subprocess.DEVNULL, check=True
        )
    except subprocess.CalledProcessError:
        raise argparse.ArgumentTypeError(f"{string} is not a Git checkout")
    else:
        return string


def arg_pofile(string):
    try:
        return _parse_pofile(string, string)
    except PoParseError as exc:
        raise argparse.ArgumentTypeError(str(exc))


def main():
    parser = argparse.ArgumentParser(description=__doc__)

    log_level_group = parser.add_mutually_exclusive_group()
    log_level_group.add_argument(
        "--quiet",
        action="store_const",
        const=logging.WARNING,
        dest="log_level",
        help="suppress informational output",
    )
    log_level_group.add_argument(
        "--debug",
        action="store_const",
        const=logging.DEBUG,
        dest="log_level",
        help="increase debug output",
    )
    parser.set_defaults(log_level=logging.INFO)

    subparsers = parser.add_subparsers(
        title="subcommands", dest="subcommand", required=True
    )

    copy_parser = subparsers.add_parser(
        "copy",
        help="Copy translations from one file to another",
        description=main_copy.__doc__,
    )
    copy_parser.set_defaults(func=main_copy)
    copy_parser.add_argument(
        "--replace-existing",
        action="store_true",
        help="replace existing translations, rather than just adding missing ones",
    )
    copy_parser.add_argument(
        "source", type=arg_pofile, help="a .po file to copy translations from"
    )
    copy_parser.add_argument(
        "dest", type=arg_pofile, help="a .po file to copy translations into"
    )

    merge_parser = subparsers.add_parser(
        "merge",
        help="Reapply downstream translation changes to a particular file",
        description=main_merge.__doc__,
    )
    merge_parser.set_defaults(func=main_merge)
    merge_parser.add_argument(
        "base",
        type=arg_pofile,
        nargs="?",
        help="ancestral upstream .po file to use as a base",
    )
    merge_parser.add_argument(
        "source", type=arg_pofile, help="a .po file to copy translations from"
    )
    merge_parser.add_argument(
        "dest", type=arg_pofile, help="a .po file to copy translations into"
    )

    rebase_parser = subparsers.add_parser(
        "rebase",
        help="Reapply downstream translation changes for one or more languages",
        description=rebase.__doc__,
    )
    rebase_parser.set_defaults(func=rebase)
    rebase_parser.add_argument(
        "-C",  # like Git's argument
        "--checkout",
        dest="git_dir",
        type=git_dir_path,
        default=os.getcwd(),
        help="path to root of Git checkout (default: current working directory)",
    )
    rebase_parser.add_argument(
        "base_rev", help="Git tag or commit SHA for ancestral upstream tag"
    )
    rebase_parser.add_argument(
        "old_rev",
        help="""Git tag or commit SHA after uploading new .pot to Transifex, pulling
        existing translations, and committing the result.""",
    )
    rebase_parser.add_argument(
        "--skip",
        action="append",
        help="Language codes to skip (if no langs are explicitly given)",
    )
    rebase_parser.add_argument(
        "langs",
        nargs="*",
        metavar="lang",
        help="""Language codes to merge (default: all from GIT_DIR/po/*.po)""",
    )

    bad_romance_parser = subparsers.add_parser(
        "bad-romance",
        help="Fill in the 'millions' plural form for Romance languages",
        description=bad_romance.__doc__,
    )
    bad_romance_parser.set_defaults(func=bad_romance)
    bad_romance_parser.add_argument(
        "-C",  # like Git's argument
        "--checkout",
        dest="git_dir",
        type=git_dir_path,
        default=os.getcwd(),
        help="path to root of Git checkout (default: current working directory)",
    )
    bad_romance_parser.add_argument(
        "upstream_rev",
        help="Git tag or commit SHA containing upstream version of these translations",
    )
    bad_romance_parser.add_argument(
        "langs",
        nargs="*",
        metavar="lang",
        default=["es", "fr", "it", "pt_BR"],
        help="Language codes to merge (default: %(default)s)",
    )

    args = parser.parse_args()
    logging.basicConfig(level=args.log_level)
    args.func(args)


if __name__ == "__main__":
    main()
